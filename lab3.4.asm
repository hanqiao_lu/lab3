	.data 0x10010000	#start from the spot
array:		.space 40		
initial:	.word 4		

	.text
	.globl main

main: 	la $a0, array	#load address of array
	lw $a1, initial	# load initial into $a1

	move $t0, $a1		# j
	li $t1, 0		# i = 0
	li $t2, 10		# $t2 = 10
	
	j Loop		#go to loop

Loop: 	ble $t2, $t1, Exit	# if 10<= i, jump to exit
	sw $t0, 0($a0)		# store j in my_array
	addi $a0, $a0, 4	# $a0 <= $a0 + 4
	addi $t0, $t0, 1	# j++
	addi $t1, $t1, 1	# i++
	j Loop		# jump to loop

Exit:	jr $ra		#return from main
