	.data 0x10010000

var1: 	.word 0x3
var2:	.word 0x4
var3:	.word -0x7d3

	.text
	.globl main


main:	lw $t0, var1	# load var1 to $t0
	lw $t1, var2	# load var2 to $t1
	lw $t2, var3	# load var3 to $t2 

	slt $t3 $t1, $t0
	bgtz $t3, Else	 # go to Else if $t0 != $t1

	move $t3, $t0	# $t3 <- $t0
	sw $t1, var1	# var1 <- $t1
	sw $t3, var2	# var2 <- $t3

	beq $0, $0, Exit	#go to Exit 

Else:	sw $t2, var1	 # store the value at $t2 to var1 
	sw $t2, var2	# store the value at $t2 to var2
	

Exit:
	jr $ra		# jump to main address


