	.data 0x10000000
msg1: 	.asciiz "Please enter two integer numbers: "
msg2: 	.asciiz "I'm far away"
msg3: 	.asciiz "I'm nearby"
	.text
	.globl main

main: 	li $v0, 4			 # system call for print_str
	la $a0, msg1		 # address of string to print
	syscall

# store 2 integers

	li $v0, 5 			# system call for read_int
	syscall 			# one integer placed in $v0
	addu $t0, $v0, $0		# store in t0
	li $v0, 5 			# system call for read_int
	syscall 			# another integer placed in $v0
	addu $t1, $v0, $0

# comparision 
	bne $t0, $t1, Nearby
	li $v0, 4
	la $a0, msg2
	syscall
	j End

Nearby:	li $v0, 4 			# system call for print_string
	la $a0, msg3
	syscall
	j End

End:	jr $ra
