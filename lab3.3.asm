	.data 0x10010000

var1: 	.word 0x4

	.text
	.globl main


main:	lw $a0, var1	# load var1 to $a0
	li $a1,100
	move $t0, $a0

Loop:	ble $a1, $t0, Exit 	# exit if 100 <= i
	addi $a0, $a0,1	# var1 = var1+1
	addi $t0, $t0, 1	# i++
	j Loop

Exit:
	sw $a0, var1
	jr $ra		# jump to main address
